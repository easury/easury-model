# Easury Excel Model
Open source Excel Model used to simplify budget tracking.

![dashboard](./images/dashboard.png)

## Download
Head over to `https://easury.com/model` to download the latest
stable version, or clone this repo to get the latest updates.

```
$ git clone https://ameier38@bitbucket.org/easury/easury-model.git
$ cd easury-model
```

## Usage
Head over to [our blog](https://easury.com/blog) for instructional
videos on different use cases.